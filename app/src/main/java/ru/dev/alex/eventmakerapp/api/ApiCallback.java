package ru.dev.alex.eventmakerapp.api;


import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.dev.alex.eventmakerapp.dto.response.ApiErrorEvent;
import ru.dev.alex.eventmakerapp.dto.response.AbstractApiResponse;

/**
 * Use this class to have a callback which can be used for the api calls in {@link ApiService}.
 * Such a callback can be invalidated to not notify its caller about the api response.
 * Furthermore it handles finishing the request after the caller has handled the response.
 */
public class ApiCallback<T extends AbstractApiResponse> implements Callback<T> {

    /**
     * Indicates if the callback was invalidated.
     */
    private boolean isInvalidated;


    public ApiCallback() {
        isInvalidated = false;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        T result = response.body();
        if (response.isSuccessful() && result != null) {
//			modifyResponseBeforeDelivery(result); // Enable when needed.
                EventBus.getDefault().post(result);
            }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (!call.isCanceled() && !isInvalidated) {
            EventBus.getDefault().post(new ApiErrorEvent(t));
        }

    }


    /**
     * This is for callbacks which extend ApiCallback and want to modify the response before it is
     * delivered to the caller. It is bit different from the interceptors as it allows to implement
     * this method and change the response.
     *
     * @param result The api response.
     */
    @SuppressWarnings("UnusedParameters")
    protected void modifyResponseBeforeDelivery(T result) {
        // Do nothing here. Only for subclasses.
    }

}
