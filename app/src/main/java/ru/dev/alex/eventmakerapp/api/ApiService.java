package ru.dev.alex.eventmakerapp.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.dev.alex.eventmakerapp.dto.response.EventsResponse;

public interface ApiService {
    @POST("getEvent")
    Call<EventsResponse> getEvents();


}
