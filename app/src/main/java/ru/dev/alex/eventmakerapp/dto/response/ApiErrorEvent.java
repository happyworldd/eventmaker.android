package ru.dev.alex.eventmakerapp.dto.response;

/**
 * When any network error happens, Event is used to send to particular class from where the Network API call generated.
 */
public class ApiErrorEvent {
    private Throwable throwable;

    /**
     * Create an error event without any further error information.
     *
     * @param requestTag Identifies the request that failed.
     */
    public ApiErrorEvent() {
    }

    /**
     * Create an error event with detailed error information in the form of a RetrofitError.
     *
     * @param requestTag Identifies the request that failed.
     * @param t          An error object with detailed information.
     */
    public ApiErrorEvent( Throwable t) {
        this.throwable = t;
    }


    public Throwable getRetrofitError() {
        return throwable;
    }
}
