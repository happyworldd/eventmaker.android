package ru.dev.alex.eventmakerapp.dto.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Event {
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("description")
    private String description;
    @SerializedName("place")
    private String place;
    @SerializedName("creator")
    private String creator;
    @SerializedName("date")
    private String date;
    @SerializedName("countPlayers")
    private int countPlayers;
}
