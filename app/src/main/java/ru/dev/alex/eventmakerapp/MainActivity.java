package ru.dev.alex.eventmakerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
//
//    public static void onClick(View view) {
//        NetworkService.getInstance().getApiService().getEvents().enqueue(new Callback<Events>() {
//            @Override
//            public void onResponse(Call<Events> call, Response<Events> response) {
//                Event event = response.body();
//                textView.append(event.getId() + "\n");
//                textView.append(event.getPlace() + "\n");
//                textView.append(event.getCreator() + "\n");
//            }
//
//            @Override
//            public void onFailure(Call<Events> call, Throwable t) {
//                textView.append("Error occurred while getting request!");
//                t.printStackTrace();
//            }
//        });
//    }
}