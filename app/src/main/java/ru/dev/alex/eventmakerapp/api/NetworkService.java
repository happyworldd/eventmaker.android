package ru.dev.alex.eventmakerapp.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static final String BASE_URL = "http://172.17.85.177:8080/"; // ipconfig (default ports)
    private Retrofit mRetrofit;
    private static NetworkService mNetworkService;

    public NetworkService() {
//        mRetrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public static NetworkService getInstance() {
        if (mNetworkService == null) {
            mNetworkService = new NetworkService();
        }
        return mNetworkService;
    }

    public ApiService getApiService() {
        return mRetrofit.create(ApiService.class);
    }


}
