package ru.dev.alex.eventmakerapp.activities.custom.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.dev.alex.eventmakerapp.R;
import ru.dev.alex.eventmakerapp.dto.response.Event;

public class CustomListItemAdapter extends BaseAdapter {
    private List<Event> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListItemAdapter(List<Event> listData, Context context) {
        this.listData = listData;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();
            holder.imageBall = (ImageView) convertView.findViewById(R.id.imageBall);
            holder.event = (TextView) convertView.findViewById(R.id.eventText);
            holder.description = (TextView) convertView.findViewById(R.id.descriptionText);
            holder.place = (TextView) convertView.findViewById(R.id.placeText);
            holder.creator = (TextView) convertView.findViewById(R.id.creatorText);
            holder.playersAmount = (TextView) convertView.findViewById(R.id.playersAmountText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Event event = this.listData.get(position);
        holder.event.setText(event.getType());
        holder.description.setText("Описание: " + event.getDescription());
        holder.place.setText("Место: " + event.getPlace());
        holder.creator.setText("Создатель: " + event.getCreator());
        int imageId = this.getMipmapResIdByName("ball");

        holder.imageBall.setImageResource(imageId);

        return convertView;
    }

    // Find Image ID corresponding to the name of the image (in the directory mipmap).
    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();
        // Return 0 if not found.
        int resID = context.getResources().getIdentifier(resName , "mipmap", pkgName);
        Log.i("CustomListView", "Res Name: "+ resName+"==> Res ID = "+ resID);
        return resID;
    }

    static class ViewHolder {
        ImageView imageBall;
        TextView event;
        TextView description;
        TextView place;
        TextView creator;
        TextView playersAmount;
    }
}
