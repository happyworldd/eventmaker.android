package ru.dev.alex.eventmakerapp.api.request;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import ru.dev.alex.eventmakerapp.api.ApiCallback;
import ru.dev.alex.eventmakerapp.api.NetworkService;
import ru.dev.alex.eventmakerapp.dto.response.Event;
import ru.dev.alex.eventmakerapp.dto.response.EventsResponse;

public class EventsRequestUtil {

    private Call<EventsResponse> call;

    private ApiCallback<EventsResponse> callback;

    /**
     * Асинхронно получаем события от сервера, не тормозя ui
     */
    public void getEventsAsync() {
        System.out.println("Вызываем сервис получения событий");
        callback = new ApiCallback<>();
        call = NetworkService.getInstance().getApiService().getEvents();
        call.enqueue(callback);

    }


    }

