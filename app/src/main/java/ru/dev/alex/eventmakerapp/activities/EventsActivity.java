package ru.dev.alex.eventmakerapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.dev.alex.eventmakerapp.R;
import ru.dev.alex.eventmakerapp.activities.custom.adapters.CustomListItemAdapter;
import ru.dev.alex.eventmakerapp.api.request.EventsRequestUtil;
import ru.dev.alex.eventmakerapp.dto.response.Event;
import ru.dev.alex.eventmakerapp.dto.response.EventsResponse;
import ru.dev.alex.eventmakerapp.dto.response.ApiErrorEvent;

public class EventsActivity extends AppCompatActivity {
    private transient ArrayList<Event> eventsList;

    EventBus mEventBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        eventsList = new ArrayList<>();

        mEventBus = EventBus.getDefault();
        if (!mEventBus.isRegistered(this)) {
            mEventBus.register(this);
        }

    }

    private void fillEventsList() throws IOException {
        ListView listView = findViewById(R.id.eventsList);


        listView.setAdapter(new CustomListItemAdapter( eventsList, this));
    }


    public void getEventsButtonOnClick(View view) throws IOException {
        EventsRequestUtil eventsRequestUtil = new EventsRequestUtil();
        eventsRequestUtil.getEventsAsync();
    }

    @Subscribe
    public void onEventGetEvents(EventsResponse eventsResponse) throws IOException {
        eventsList = eventsResponse.getEventsList();
        fillEventsList();

    }

    @Subscribe
    public void onApiErrorEvent(ApiErrorEvent apiErrorEvent) {
        //если отвалимся по ошибке,  то нужно че нибудь тут сделать
    }


}
