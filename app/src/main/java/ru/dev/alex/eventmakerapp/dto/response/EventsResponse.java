package ru.dev.alex.eventmakerapp.dto.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class EventsResponse extends AbstractApiResponse {
    @SerializedName("eventsList")
    private ArrayList<Event> eventsList;

}
