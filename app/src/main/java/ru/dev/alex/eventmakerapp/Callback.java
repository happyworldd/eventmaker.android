package ru.dev.alex.eventmakerapp;

public interface Callback<T> {
    void next(T result);
}
